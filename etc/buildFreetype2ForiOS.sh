#!/bin/bash

# Original script from here: http://stackoverflow.com/a/27161949/1527560
# Many thanks to "Mobile Ben"!

# Modified to build freetype2 by alejandro (Updated 21-11-2019)

# Builds properly on MacOS Catalina with SDK 13.2

# I recommend to build only one platform first, to make sure everything is working fine (disable all the others).
# Then enable all the others again and let the script work on its own
# Note that the last step, LIPO(3) will ALWAYS fail until you enable ALL the architectures!

# Latest compiled version:  freetype-2.10.1
# Changed -fembed-bitcode-marker to -fembed-bitcode, otherwise the project cannot be archived and therefore cannot be sent to the AppStore.

# Additional notes
# For arm7, arm7s, i386, the most recent SDKMINVERSION is 10
# thereafter it's 64-bits

# info: https://en.wikipedia.org/wiki/Apple-designed_processors
# see also: https://en.wikipedia.org/wiki/IOS_version_history#iOS_10

# For now minSDK version is 10, until Apple gets crazy about it
#
# Do note that disabling/enabling instruction sets in the BUILD(2) step
#  requires you to also remove/add the instruction set in the LIPO(3) step

### Good luck ####

VERBOSE=" " #"-v" #Leave the space if you don't want verbose, otherwise it will fail!!
SDKMINVERSION="10.0"

export IPHONEOS_DEPLOYMENT_TARGET="13.2"


## Step 1 Configuration(1)

PLATFORMPATH="/Applications/Xcode.app/Contents/Developer/Platforms"
TOOLSPATH="/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin"

pwd=`pwd`

findLatestSDKVersion()
{
  sdks=`ls $PLATFORMPATH/$1.platform/Developer/SDKs`
  arr=()
  for sdk in $sdks
  do
    arr[${#arr[@]}]=$sdk
  done

  # Last item will be the current SDK, since it is alpha ordered
  count=${#arr[@]}
  if [ $count -gt 0 ]; then
    sdk=${arr[$count-1]:${#1}}
    num=`expr ${#sdk}-4`
    SDKVERSION=${sdk:0:$num}
  else
    SDKVERSION=$SDKMINVERSION
  fi
}

buildit()
{
  target=$1
  hosttarget=$1
  platform=$2

  if [[ $hosttarget == "x86_64" ]]; then
    hostarget="i386"
  elif [[ $hosttarget == "arm64" ]]; then
    hosttarget="arm"
  fi

  export CC="$(xcrun -sdk iphoneos -find clang)"
  export CPP="$CC -E"
  export CFLAGS="$VERBOSE -arch ${target} -fembed-bitcode -isysroot $PLATFORMPATH/$platform.platform/Developer/SDKs/$platform$SDKVERSION.sdk -miphoneos-version-min=$SDKMINVERSION"
  export AR=$(xcrun -sdk iphoneos -find ar)
  export RANLIB=$(xcrun -sdk iphoneos -find ranlib)
  export CPPFLAGS="$VERBOSE -arch ${target} -fembed-bitcode -isysroot $PLATFORMPATH/$platform.platform/Developer/SDKs/$platform$SDKVERSION.sdk -miphoneos-version-min=$SDKMINVERSION"
  export LDFLAGS="$VERBOSE -arch ${target} -isysroot $PLATFORMPATH/$platform.platform/Developer/SDKs/$platform$SDKVERSION.sdk -miphoneos-version-min=$SDKMINVERSION"

  mkdir -p $pwd/output/$target

  #echo "*** isysroot is $PLATFORMPATH/$platform.platform/Developer/SDKs/$platform$SDKVERSION.sdk ***"

  ./configure $VERBOSE --prefix="$pwd/output/$target" --disable-shared --without-png --without-harfbuzz --host=$hosttarget-apple-darwin

  make clean
  make
  make install
}

findLatestSDKVersion iPhoneOS

## Step 2 Build(2)

buildit armv7 iPhoneOS # Builds with SDKMINVERSION up to 10
buildit armv7s iPhoneOS # Builds with SDKMINVERSION up to 10
buildit i386 iPhoneSimulator # Builds with SDKMINVERSION up to 10

buildit arm64 iPhoneOS # Builds with SDKMINVERSION all the way up to 13
buildit x86_64 iPhoneSimulator # Builds with SDKMINVERSION all the way up to 13


## Step 3 Lipo(3)

LIPO=$(xcrun -sdk iphoneos -find lipo)

$LIPO -create $pwd/output/armv7/lib/libfreetype.a  $pwd/output/armv7s/lib/libfreetype.a $pwd/output/arm64/lib/libfreetype.a $pwd/output/x86_64/lib/libfreetype.a $pwd/output/i386/lib/libfreetype.a -output libfreetype.a
#$LIPO -create $pwd/output/x86_64/lib/libfreetype.a $pwd/output/arm64/lib/libfreetype.a -output libfreetype.a

echo "* Completed script *"
