
# Copyright (C) 2014-2020 Alejandro Valenzuela Roca
#

PREFIX := /media/exphat/common/fh/takkatakka/takkatakkacpp/

# Only the game will be compiled. The engine will be loaded as a static library.
#
# -------------

# no idea what this does but if you remove it the world ends so better not touch it
# ndk-build works in mysterious ways, whatever
LOCAL_PATH:= $(call my-dir)




# getting libPNG into the project
include $(CLEAR_VARS)

LOCAL_MODULE    := png16
LOCAL_SRC_FILES := precompiled/png/android/$(TARGET_ARCH_ABI)/libpng16.a

include $(PREBUILT_STATIC_LIBRARY)
# -------------

# getting freetype2 into the project
include $(CLEAR_VARS)

LOCAL_MODULE    := freetype2
LOCAL_SRC_FILES := precompiled/freetype2/android/$(TARGET_ARCH_ABI)/libfreetype.a

include $(PREBUILT_STATIC_LIBRARY)
# -------------

# getting libmjEngine into the project
include $(CLEAR_VARS)

LOCAL_MODULE    := libmjEngine
LOCAL_SRC_FILES := precompiled/mjEngine/android/$(TARGET_ARCH_ABI)/libmjEngine.a

include $(PREBUILT_STATIC_LIBRARY)
# -------------

# Now, the files from the game are specified:

include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/extLibs/freetype2/include

LOCAL_MODULE    := libmjGame
LOCAL_SRC_FILES := \
    $(PREFIX)/TakkaTakkaGameState.cpp \
    $(PREFIX)/gameModes/ChallengeGameMode.cpp \
    $(PREFIX)/gameModes/GameMode.cpp \
    $(PREFIX)/gameModes/PartyGameMode.cpp \
    $(PREFIX)/gameObjects/BaitAndSwitchTrackEvent.cpp \
    $(PREFIX)/gameObjects/Bubble.cpp \
    $(PREFIX)/gameObjects/BubbleExplosion.cpp \
    $(PREFIX)/gameObjects/BubbleFragment.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/BaitAndSwitchBubbleScript.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/BaitAndSwitchBubbleScriptSettings.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/BeatZigZagBubbleScript.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/BeatZigZagBubbleScriptSettings.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/BubbleScript.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/BubbleScriptSettings.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/SpiralWalkBubbleScript.cpp \
    $(PREFIX)/gameObjects/BubbleScripts/SpiralWalkBubbleScriptSettings.cpp \
    $(PREFIX)/gameObjects/Choreographer.cpp \
    $(PREFIX)/gameObjects/Draggable.cpp \
    $(PREFIX)/gameObjects/Player.cpp \
    $(PREFIX)/gameObjects/Poke.cpp \
    $(PREFIX)/gameObjects/Step.cpp \
    $(PREFIX)/gameObjects/Track.cpp \
    $(PREFIX)/gameObjects/TrackEvent.cpp \
    $(PREFIX)/gameObjects/TrackLoader.cpp \
    $(PREFIX)/gameObjects/TrackPreview.cpp \
    $(PREFIX)/gl_code.cpp \
    $(PREFIX)/interfaceObjects/HealthBar.cpp \
    $(PREFIX)/interfaceObjects/PictureButton.cpp \
    $(PREFIX)/interfaceObjects/ResponseText.cpp \
    $(PREFIX)/interfaceObjects/ToggleButton.cpp \
    $(PREFIX)/interfaceObjects/TrackSelector.cpp \
    $(PREFIX)/miscObjects/BackgroundObject.cpp \
    $(PREFIX)/pools/BackgroundBubblePool.cpp \
    $(PREFIX)/pools/BubbleFragmentPool.cpp \
    $(PREFIX)/shaders/BubbleShaders.cpp \
    $(PREFIX)/subsystems/Background.cpp \
    $(PREFIX)/subsystems/InterfaceBuilder.cpp \
    $(PREFIX)/subsystems/TokenStore.cpp \
    $(PREFIX)/subsystems/TrackList.cpp \
    $(PREFIX)/subsystems/TrackListLoader.cpp \
    $(PREFIX)/universes/ChooserUniverse.cpp \
    $(PREFIX)/universes/CreditsUniverse.cpp \
    $(PREFIX)/universes/SlideTransitionUniverse.cpp \
    $(PREFIX)/universes/SongDetailsUniverse.cpp \
    $(PREFIX)/universes/SongUniverse.cpp \
    $(PREFIX)/universes/TitleUniverse.cpp

# -------------

# now, the shared and static libs to be used are specified
LOCAL_LDLIBS    := -llog -lGLESv2 -lz -landroid
LOCAL_STATIC_LIBRARIES := libmjEngine png16 freetype2

# finally we tell ndk-build that the output is a shared library. This will be loaded by Java in the android device
include $(BUILD_SHARED_LIBRARY)
