#!/bin/sh

# Script to "easily" build external libraries using the Android toolchain
#
# Note, as of NDK 21.3.6528147, the AR, AS, LD, RANLIB and STRIP commands have erroneous names not fitting the pattern:
# e.g. arm-linux-androideabi-ar should actually be armv7a-linux-androideabi-arm, and so on.
#
# So there's a specific fix for that later in the code.
#


# Set specific "configure" options here. They will be added when ./configure is invoked.

export CONFIGURE_EXTRAS="--disable-shared --without-png --without-harfbuzz --without-brotli" # hint: for libpng leave empty; for freetype, use "--disable-shared --without-png --without-harfbuzz" 

# Set NDK location here
export NDK=/common/tools/sdk/ndk/21.3.6528147


# Set this to your minSdkVersion.
export API=21


# Only choose one of these, depending on your build machine...
# export TOOLCHAIN=$NDK/toolchains/llvm/prebuilt/darwin-x86_64
export TOOLCHAIN=$NDK/toolchains/llvm/prebuilt/linux-x86_64



#create general output dir
mkdir ./androidLibraries

for TARGET in armv7a-linux-androideabi i686-linux-android x86_64-linux-android aarch64-linux-android #armv7a-linux-androideabi
do
echo "Building for $TARGET"

# Configure and build.
export AR=$TOOLCHAIN/bin/$TARGET-ar
export AS=$TOOLCHAIN/bin/$TARGET-as
export CC=$TOOLCHAIN/bin/$TARGET$API-clang
export CXX=$TOOLCHAIN/bin/$TARGET$API-clang++
export LD=$TOOLCHAIN/bin/$TARGET-ld
export RANLIB=$TOOLCHAIN/bin/$TARGET-ranlib
export STRIP=$TOOLCHAIN/bin/$TARGET-strip


# Some quick substitutions in case the NDK people misnamed their tools... again -_-'
# clang seems to be OK
if test ! -f "$AR"; then
 export AR=$TOOLCHAIN/bin/arm-linux-androideabi-ar
fi

if test ! -f "$AS"; then
 export AS=$TOOLCHAIN/bin/arm-linux-androideabi-as
fi

if test ! -f "$LD"; then
 export LD=$TOOLCHAIN/bin/arm-linux-androideabi-ld
fi

if test ! -f "$RANLIB"; then
 export RANLIB=$TOOLCHAIN/bin/arm-linux-androideabi-ranlib
fi

if test ! -f "$STRIP"; then
 export STRIP=$TOOLCHAIN/bin/arm-linux-androideabi-strip
fi


./configure --host=$TARGET $CONFIGURE_EXTRAS
make clean
make

mkdir ./androidLibraries/$TARGET

echo "Copying libraries.."

if test -f "./.libs"; then
   cp ./.libs/*.a ./androidLibraries/$TARGET
else
   cp ./objs/.libs/*.a ./androidLibraries/$TARGET
fi


echo "End of $TARGET"

done

echo "Done."
