#ifndef UDPRECEIVER_H
#define UDPRECEIVER_H

#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>
#include "../extLibs/logger/mjLog.h"

namespace mjEngine {


class mjUDPSocket
{
private:
    size_t bufferLength = 512;
    socklen_t addressStructureLength;

    uint16_t port;
    struct sockaddr_in socketStructureSelf, socketStructureSender;
    struct addrinfo* targetAddressResolved;


    char* buffer;

    void Init();
public:
    int socketFileDescriptor = -1;

    mjUDPSocket();
    mjUDPSocket(uint16_t port);
    virtual ~mjUDPSocket();
    int Bind();
    ssize_t Update();
    int SetTarget(char* targetAddress);
    ssize_t SendData(size_t dataLength, char* buffer);
    ssize_t SendDataTo(size_t dataLength, char* buffer, sockaddr_in& target);
    virtual void OnDataReceived(ssize_t receivedLength, char* buffer, sockaddr_in& socketStructureSender) = 0;
};

}
#endif // UDPRECEIVER_H
