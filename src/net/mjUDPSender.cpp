#include "mjUDPSender.h"

namespace mjEngine {

mjUDPSender::mjUDPSender()
{
    Init(-1);
}

mjUDPSender::mjUDPSender(uint16_t port)
{
    this->port = port;
    Init(-1);
}

mjUDPSender::mjUDPSender(mjUDPSocket* sharedSocket)
{
    //LOGI("SharedSocket FD inside udpsender: %d gsgsd", sharedSocket->socketFileDescriptor);
    Init(sharedSocket->socketFileDescriptor);
}

mjUDPSender::~mjUDPSender()
{
    delete [] buffer;
}

int mjUDPSender::SetTarget(char* targetAddress)
{
    struct addrinfo hints;
    struct addrinfo* targetAddressResolved;

    memset( &hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    return getaddrinfo(targetAddress, NULL, &hints, &targetAddressResolved);
}

ssize_t mjUDPSender::SendData(size_t dataLength, char* buffer)
{
    return sendto(socketFileDescriptor, buffer, dataLength, 0, reinterpret_cast<struct sockaddr *>(&addressStructureTarget), addressStructureLength);
}

ssize_t mjUDPSender::SendDataTo(size_t dataLength, char* buffer, sockaddr_in& target)
{
    return sendto(socketFileDescriptor, buffer, dataLength, 0, reinterpret_cast<struct sockaddr *>(&target), addressStructureLength);
    //LOGI("sendto: %d", res);

}
void mjUDPSender::Init(int sharedSocketFileDescriptor) {

    addressStructureLength = sizeof (addressStructureTarget);
    buffer = new char[bufferLength];


    // Clear out the structures
    memset(reinterpret_cast<char *>(&addressStructureSelf), 0, addressStructureLength);

    if (sharedSocketFileDescriptor == -1) {
        socketFileDescriptor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        // Set non-blocking
        fcntl(socketFileDescriptor, F_SETFL, O_NONBLOCK);
    } else {
        socketFileDescriptor = sharedSocketFileDescriptor;
    }
}

}
