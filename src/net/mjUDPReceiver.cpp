#include "mjUDPReceiver.h"

namespace mjEngine {

mjUDPSocket::mjUDPSocket()
{
    Init();
}

mjUDPSocket::mjUDPSocket(uint16_t port)
{
    this->port = port;
    Init();
}

mjUDPSocket::~mjUDPSocket()
{
    delete [] buffer;
}
void mjUDPSocket::Init() {

    addressStructureLength = sizeof (socketStructureSender);



    // Clear out the structures
    memset(reinterpret_cast<char *>(&socketStructureSelf), 0, addressStructureLength);
    socketFileDescriptor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    // Set non-blocking
    fcntl(socketFileDescriptor, F_SETFL, O_NONBLOCK);

    //LOGI("socketFileDescripor at creation time: %d", socketFileDescriptor);

    buffer = new char[bufferLength];
}

int mjUDPSocket::Bind()
{
    socketStructureSelf.sin_family = AF_INET;
    socketStructureSelf.sin_port = htons(port);
    socketStructureSelf.sin_addr.s_addr = htonl(INADDR_ANY);

    return bind(socketFileDescriptor, reinterpret_cast<struct sockaddr*>(&socketStructureSelf), addressStructureLength);
}

ssize_t mjUDPSocket::Update()
{
    ssize_t receivedLength = recvfrom(socketFileDescriptor, buffer, bufferLength, 0, reinterpret_cast<struct sockaddr*>(&socketStructureSender), &addressStructureLength);

    if (receivedLength != -1) {
        this->OnDataReceived(receivedLength, buffer, socketStructureSender);
    }
    return receivedLength;
}

int mjUDPSocket::SetTarget(char* targetAddress)
{
    struct addrinfo hints;

    memset( &hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    return getaddrinfo(targetAddress, NULL, &hints, &targetAddressResolved);
}

ssize_t mjUDPSocket::SendData(size_t dataLength, char* buffer)
{
    return sendto(socketFileDescriptor, buffer, dataLength, 0, reinterpret_cast<struct sockaddr *>(targetAddressResolved), addressStructureLength);
}

ssize_t mjUDPSocket::SendDataTo(size_t dataLength, char* buffer, sockaddr_in& target)
{
    return sendto(socketFileDescriptor, buffer, dataLength, 0, reinterpret_cast<struct sockaddr *>(&target), addressStructureLength);
    //LOGI("sendto: %d", res);

}

}
