#ifndef UDPSENDER_H
#define UDPSENDER_H

#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#include <fcntl.h>

#include "mjUDPReceiver.h"
#include "../extLibs/logger/mjLog.h"

namespace mjEngine {


class mjUDPSender
{
private:
    size_t bufferLength = 512;
    unsigned addressStructureLength;

    uint16_t port;
    struct sockaddr_in addressStructureSelf, addressStructureTarget;

    char* buffer;

    void Init(int sharedSocketFileDescriptor);
public:
    int socketFileDescriptor = -1;

    mjUDPSender();
    mjUDPSender(uint16_t port);
    mjUDPSender(mjUDPSocket* sharedSocket);
    virtual ~mjUDPSender();
    int SetTarget(char* targetAddress);
    ssize_t SendData(size_t dataLength, char* buffer);
    ssize_t SendDataTo(size_t dataLength, char* buffer, struct sockaddr_in& target);
};

}
#endif // UDPSender_H
