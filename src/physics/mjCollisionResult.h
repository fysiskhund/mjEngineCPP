/*
 * Copyright (C) 2014 Alejandro Valenzuela Roca
 */

#ifndef MJCOLLISIONRESULT_H
#define MJCOLLISIONRESULT_H

#include "mjPhysicsEffect.h"
#include "../core/mjConstants.h"

namespace mjEngine{

enum MJ_COLLISION_RESULT_SIMPLE{
	MJ_NO_COLLISION=0,
	MJ_OVERLAP,
	MJ_UNDER,
	MJ_OVER
};

class mjCollisionResult{
public:
    MJ_COLLISION_RESULT_SIMPLE result = MJ_NO_COLLISION;
    mjPhysicsEffect relocationEffectObj0;
    mjPhysicsEffect changeVelEffectObj0;
    mjPhysicsEffect accelObj0;
    mjPhysicsEffect relocationEffectObj1;
    mjPhysicsEffect changeVelEffectObj1;
    mjPhysicsEffect accelObj1;
    float dist = MJ_OVER_9000;
    void Reset();
};

}

#endif
