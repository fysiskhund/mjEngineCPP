#ifndef MJCOLLISIONRESULTPOOLHOLDER_H
#define MJCOLLISIONRESULTPOOLHOLDER_H

#include <vector>
#include "mjCollisionResult.h"

namespace mjEngine {


class mjCollisionResultPoolHolder
{
public:
    unsigned* currentColResultAvailableIndex = nullptr;

    std::vector<mjCollisionResult*>* colResultPool = nullptr;

    mjCollisionResult* current = nullptr;

    std::vector<mjCollisionResult*> newCollisionResults;

    mjCollisionResult* GetNextAvailableCollisionResult();
};

}
#endif // MJCOLLISIONRESULTPOOLHOLDER_H
