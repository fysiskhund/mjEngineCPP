#include "mjCollisionResultPoolHolder.h"

namespace mjEngine {


mjCollisionResult* mjCollisionResultPoolHolder::GetNextAvailableCollisionResult()
{
    mjCollisionResult* colResult;
    // Need a new colResult. Reuse something from the pool, or get a new one
    if ((*currentColResultAvailableIndex) < colResultPool->size())
    {
        colResult = (*colResultPool)[(*currentColResultAvailableIndex)];
        colResult->Reset();
    } else
    {
        colResult = new mjCollisionResult();
        LOGI("%s %d: new %s", __FILE__, __LINE__, "collisionResult for pool");
        colResultPool->push_back(colResult);

    }
    (*currentColResultAvailableIndex)++;
    return colResult;
}

}
