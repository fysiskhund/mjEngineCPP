#include "mjCollisionTests.h"

namespace mjEngine{

// The FUZZ value is an Epsilon to avoid "vibration" due to floating point imprecisions and achieve practical results,
// The value chosen is a less than a tenth of a millimetre, which is for most game purposes perfectly fine.

#define FUZZ 0.00005


MJ_COLLISION_RESULT_SIMPLE mjCollisionTests::SphereVsSphere(mjSphere* s0, mjSphere* s1, mjCollisionResult *out){


	mjVector3 s0ToS1;

	//LOGI("in SphvSph; s0 : 0x%x : %3.3f,%3.3f,%3.3f ", s0->c, s0->c->x, s0->c->y, s0->c->z);
	//LOGI("in SphvSph; s1 : 0x%x : %3.3f,%3.3f,%3.3f ", s1->c, s1->c->x, s1->c->y, s1->c->z);

	s0ToS1.CopyFrom(*s1->c);

	//LOGI("in SphvSph; s0ToS1 : 0x%x : %3.3f,%3.3f,%3.3f ", &s0ToS1, s0ToS1.x, s0ToS1.y, s0ToS1.z);
	s0ToS1.Subtract(*s0->c);

	//LOGI("in SphvSph; s0ToS1 : 0x%x : %3.3f,%3.3f,%3.3f ", &s0ToS1, s0ToS1.x, s0ToS1.y, s0ToS1.z);


	float dist = s0ToS1.Normalize();
	//LOGI("after Normalise");
	float rSum = s0->r+s1->r;

	//LOGI("dist %3.3f, rSum %3.3f", dist, rSum);
	if (dist < rSum)
	{

		if (out != NULL)
		{
                        out->dist = dist;
			float minDistHalf = rSum*0.5;

			//LOGI("minDistHalf = %3.3f", minDistHalf);
            //out->relocationEffectObj0 = new mjPhysicsEffect();
            out->relocationEffectObj0.type = MJ_COLLISION;
            out->relocationEffectObj0.action = MJ_CHANGE_POSITION;

			s0ToS1.MulScalar(-minDistHalf);

            out->relocationEffectObj0.value.CopyFrom(s0ToS1);




            //out->relocationEffectObj1 = new mjPhysicsEffect();
            out->relocationEffectObj1.type = MJ_COLLISION;
            out->relocationEffectObj1.action = MJ_CHANGE_POSITION;

			s0ToS1.MulScalar(-1);

            out->relocationEffectObj1.value.CopyFrom(s0ToS1);
			out->result= MJ_OVERLAP;

		}
		//LOGI("Returning OVERLAP");
		return MJ_OVERLAP;
	} else
	{
		//LOGI("Returning NOCOLLISION, d:%3.3f", dist);
		return MJ_NO_COLLISION;
	}

}




MJ_COLLISION_RESULT_SIMPLE mjCollisionTests::AABBVsAABB(mjAABB* aabb0, mjAABB* aabb1, mjCollisionResult* out)
	{

		if (( aabb0->minCorner.x -  aabb1->maxCorner.x > FUZZ) || (  aabb0->minCorner.y -  aabb1->maxCorner.y > FUZZ) || (  aabb0->minCorner.z -  aabb1->maxCorner.z > FUZZ) ||
		    ( aabb1->minCorner.x -  aabb0->maxCorner.x > FUZZ) || (  aabb1->minCorner.y -  aabb0->maxCorner.y > FUZZ) || (  aabb1->minCorner.z -  aabb0->maxCorner.z > FUZZ))
		{
			/*LOGI("[%3.3f, %3.3f, %3.3f ; %3.3f, %3.3f, %3.3f] vs [%3.3f, %3.3f, %3.3f ; %3.3f, %3.3f, %3.3f]",
					aabb0->minCorner.x, aabb0->minCorner.y, aabb0->minCorner.z,
					aabb0->maxCorner.x, aabb0->maxCorner.y, aabb0->maxCorner.z,

					aabb1->minCorner.x, aabb1->minCorner.y, aabb1->minCorner.z,
					aabb1->maxCorner.x, aabb1->maxCorner.y, aabb1->maxCorner.z);

			LOGI("%d %d %d\n%d %d %d",
					 (aabb0->minCorner.x > aabb1->maxCorner.x),
					 (aabb0->minCorner.y > aabb1->maxCorner.y),
					 (aabb0->minCorner.z > aabb1->maxCorner.z),

					 (aabb1->minCorner.x > aabb0->maxCorner.x),
					 (aabb1->minCorner.y > aabb0->maxCorner.y),
					 (aabb1->minCorner.z > aabb0->maxCorner.z)
					 );*/
			//LOGI("No collision AABB");
			return MJ_NO_COLLISION;
		} else
		{
			//LOGI("COLLISION!!AABB");
			MJ_COLLISION_RESULT_SIMPLE result = MJ_OVERLAP;


			if (out != NULL)
			{
                out->result = result;
				/*out->relocationEffectObj0 = new mjPhysicsEffect();
				out->relocationEffectObj1 = new mjPhysicsEffect();*/
				// Get the directions for moving the boxes
				mjVector3 directions;
				directions.CopyFrom(*aabb0->center);
				directions.Subtract(*aabb1->center);

				mjVector3 maxEndp;
				maxEndp.Set(fmin(aabb0->maxCorner.x, aabb1->maxCorner.x), fmin(aabb0->maxCorner.y, aabb1->maxCorner.y),
							fmin(aabb0->maxCorner.z, aabb1->maxCorner.z));
				mjVector3 minEndp;
				minEndp.Set(fmax(aabb0->minCorner.x, aabb1->minCorner.x), fmax(aabb0->minCorner.y, aabb1->minCorner.y),
						fmax(aabb0->minCorner.z, aabb1->minCorner.z));

				mjVector3 overlap;
				overlap.CopyFrom(maxEndp);
				overlap.Subtract(minEndp);


                //out->relocationEffectObj0 = new mjPhysicsEffect(MJ_COLLISION, MJ_CHANGE_POSITION);
                //out->changeVelEffectObj0 = new mjPhysicsEffect(MJ_COLLISION, MJ_ADD_VELOCITY);
                out->relocationEffectObj0.type = MJ_COLLISION;
                out->relocationEffectObj0.action = MJ_CHANGE_POSITION;
                out->changeVelEffectObj0.type = MJ_COLLISION;
                out->changeVelEffectObj0.action = MJ_ADD_VELOCITY;

                //out->relocationEffectObj1 = new mjPhysicsEffect(MJ_COLLISION, MJ_CHANGE_POSITION);
                //out->changeVelEffectObj1 = new mjPhysicsEffect(MJ_COLLISION, MJ_ADD_VELOCITY);
                out->relocationEffectObj1.type = MJ_COLLISION;
                out->relocationEffectObj1.action = MJ_CHANGE_POSITION;
                out->changeVelEffectObj1.type = MJ_COLLISION;
                out->changeVelEffectObj1.action = MJ_ADD_VELOCITY;



                mjVector3* location0 = &out->relocationEffectObj0.value;
                mjVector3* location1 = &out->relocationEffectObj1.value;

				float displacementFactor = 0.5f;

				if (aabb0->isImmovable || aabb1->isImmovable)
				{
					displacementFactor = 1;
					//LOGI("dispFactor:1");
				}

				if ((overlap.x < overlap.y) && (overlap.x < overlap.z))
				{
                    out->relocationEffectObj0.mask[0] = mjMathHelper::Sign(directions.x);

                    if (out->relocationEffectObj0.mask[0] == 0) out->relocationEffectObj0.mask[0] = 1;

                    out->relocationEffectObj1.mask[0] = -out->relocationEffectObj0.mask[0];

                    float xDisplacement = out->relocationEffectObj0.mask[0]*(aabb0->halfWidths.x + aabb1->halfWidths.x)*displacementFactor;
					location0->x = aabb1->center->x + xDisplacement;
					location1->x = aabb0->center->x - xDisplacement;



                    out->changeVelEffectObj0.value.Set(out->relocationEffectObj0.mask[0], 0, 0);
                    out->changeVelEffectObj1.value.Set(out->relocationEffectObj1.mask[0], 0, 0);


                    out->relocationEffectObj0.mask[1] = false;
                    out->relocationEffectObj0.mask[2] = false;
                    out->changeVelEffectObj0.mask[1] = false;
                    out->changeVelEffectObj0.mask[2] = false;

                    out->relocationEffectObj1.mask[1] = false;
                    out->relocationEffectObj1.mask[2] = false;
                    out->changeVelEffectObj1.mask[1] = false;
                    out->changeVelEffectObj1.mask[2] = false;
				} else if ((overlap.y < overlap.x ) && (overlap.y < overlap.z))
				{
                    out->relocationEffectObj0.mask[1] = mjMathHelper::Sign(directions.y);

                    if (out->relocationEffectObj0.mask[1] == 0) out->relocationEffectObj0.mask[1] = 1;

                    float yDisplacement = out->relocationEffectObj0.mask[1]*(aabb0->halfWidths.y + aabb1->halfWidths.y)*displacementFactor;
					location0->y = aabb1->center->y + yDisplacement;
					location1->y = aabb0->center->y - yDisplacement;



                    out->relocationEffectObj1.mask[1] = -out->relocationEffectObj0.mask[1];

                    out->changeVelEffectObj0.value.Set(0, out->relocationEffectObj0.mask[1], 0);
                    out->changeVelEffectObj1.value.Set(0, out->relocationEffectObj1.mask[1], 0);


                    out->relocationEffectObj0.mask[0] = false;
                    out->relocationEffectObj0.mask[2] = false;
                    out->changeVelEffectObj0.mask[0] = false;
                    out->changeVelEffectObj0.mask[2] = false;

                    out->relocationEffectObj1.mask[0] = false;
                    out->relocationEffectObj1.mask[2] = false;
                    out->changeVelEffectObj1.mask[0] = false;
                    out->changeVelEffectObj1.mask[2] = false;
				} else
				{
                    out->relocationEffectObj0.mask[2] = mjMathHelper::Sign(directions.z);

                    if (out->relocationEffectObj0.mask[2] == 0) out->relocationEffectObj0.mask[2] = 1;

                    out->relocationEffectObj1.mask[2] = -out->relocationEffectObj0.mask[2];

                    float zDisplacement = out->relocationEffectObj0.mask[2]*(aabb0->halfWidths.z + aabb1->halfWidths.z)*displacementFactor;

					location0->z = aabb1->center->z + zDisplacement;
					location1->z = aabb0->center->z - zDisplacement;



                    out->changeVelEffectObj0.value.Set(0, 0, out->relocationEffectObj0.mask[2]);
                    out->changeVelEffectObj1.value.Set(0, 0, out->relocationEffectObj1.mask[2]);

                    out->relocationEffectObj0.mask[0] = false;
                    out->relocationEffectObj0.mask[1] = false;
                    out->changeVelEffectObj0.mask[0] = false;
                    out->changeVelEffectObj0.mask[1] = false;

                    out->relocationEffectObj1.mask[0] = false;
                    out->relocationEffectObj1.mask[1] = false;
                    out->changeVelEffectObj1.mask[0] = false;
                    out->changeVelEffectObj1.mask[1] = false;
				}
				//out->relocationEffectObj1->value.Check(__FILE__, __LINE__);
				/*LOGI("displacement obj0: %3.3f, %3.3f, %3.3f",
						out->relocationEffectObj0->value.x,
						out->relocationEffectObj0->value.y,
						out->relocationEffectObj0->value.z
						);*/


			}

			return result;
		}


	}


MJ_COLLISION_RESULT_SIMPLE mjCollisionTests::SphereVsAABB(mjSphere* s0, mjAABB* aabb0, mjCollisionResult* out)
{
    // Test distance from centers to be > (r + halfWidths)


    mjVector3 aabb0cS0c;

    aabb0cS0c.CopyFrom(*aabb0->center);
    aabb0cS0c.Subtract(*s0->c);



    float dist = aabb0cS0c.GetNorm();

    mjVector3 overlap;

    overlap.CopyFrom(aabb0cS0c);

    overlap.x -= s0->r+aabb0->halfWidths.x;
    overlap.y -= s0->r+aabb0->halfWidths.y;
    overlap.z -= s0->r+aabb0->halfWidths.z;



    if (overlap.x < 0 && overlap.y < 0 && overlap.z < 0)
    {
        float displacementFactor = 0.5;
        if (s0->isImmovable || aabb0->isImmovable)
		{
					displacementFactor = 1;
					//LOGI("dispFactor:1");
        }


        if (out != NULL)
        {


            if ((overlap.x < overlap.y) && (overlap.x < overlap.z))
            {

            } else if ((overlap.y < overlap.x ) && (overlap.y < overlap.z))
            {

            } else
            {

            }

        }
        return MJ_OVERLAP;
    } else
        return MJ_NO_COLLISION;



}

MJ_COLLISION_RESULT_SIMPLE mjCollisionTests::RayVsTriangle(mjRay* ray, mjTriangle* tr, mjCollisionResult* out)
{
    // Stolen from
    // https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
    MJ_COLLISION_RESULT_SIMPLE colResult;
    const float EPSILON = 0.0000001;
    mjVector3 edge1(*tr->v1);
    mjVector3 edge2(*tr->v2);
    mjVector3 h;
    mjVector3 s;
    mjVector3 q;
    float a,f,u,v;

    edge1.Subtract(*tr->v0);
    edge2.Subtract(*tr->v0);
    ray->dir.CrossOut(edge2, &h);
    a = edge1.Dot(h);
    if (a > -MJ_EPSILON && a < MJ_EPSILON)
        return MJ_NO_COLLISION;    // This ray is parallel to this triangle.

    f = 1.0/a;
    s.CopyFrom(*ray->origin);
    s.Subtract(*tr->v0);
    u = f * (s.Dot(h));

    if (u < 0.0 || u > 1.0)
        return MJ_NO_COLLISION;

    s.CrossOut(edge1, &q);
    v = f * ray->dir.Dot(q);
    if (v < 0.0 || u + v > 1.0)
        return MJ_NO_COLLISION;

    // At this stage we can compute t to find out where the intersection point is on the line.
    float t = f * edge2.Dot(q);
    if (t > 0) { // ray intersection
        colResult = MJ_OVER;
    } else  { // This means that there is a line intersection but not a ray intersection.
        // In motorJ we return "UNDER" because it's still useful.
        colResult = MJ_UNDER;
    }

    if (out != NULL) {
        out->relocationEffectObj0.type = MJ_COLLISION;
        out->relocationEffectObj0.action = MJ_CHANGE_POSITION;
        out->changeVelEffectObj0.type = MJ_COLLISION;
        out->changeVelEffectObj0.action = MJ_ADD_VELOCITY;

        /* FIXME:
         *
         * When collision response for meshes is implemented, uncomment this part.
         * Otherwise it's just doing operations that will never be used

        out->relocationEffectObj1 = new mjPhysicsEffect(MJ_COLLISION, MJ_CHANGE_POSITION);
        out->changeVelEffectObj1 = new mjPhysicsEffect(MJ_COLLISION, MJ_ADD_VELOCITY);
        out->relocationEffectObj1.type = MJ_COLLISION;
        out->relocationEffectObj1.action = MJ_CHANGE_POSITION;
        out->changeVelEffectObj1.type = MJ_COLLISION;
        out->changeVelEffectObj1.action = MJ_ADD_VELOCITY;

        out->relocationEffectObj1.value.CopyFrom(ray->dir);
        out->relocationEffectObj1.value.MulScalar(-t); */

        out->relocationEffectObj0.value.CopyFrom(ray->dir);
        out->relocationEffectObj0.value.MulScalar(t);

        // copy the triangle's normal
        tr->Normal(&out->changeVelEffectObj0.value);



        out->dist = t;
        out->result = colResult;
    }
    return colResult;


}

MJ_COLLISION_RESULT_SIMPLE mjCollisionTests::RayVsMeshBS(mjRay* ray, mjMeshBS* meshBS, mjCollisionResultPoolHolder* poolHolder)
{
    //LOGI("Start of test %3.3f, %3.3f, %3.3f", ray->origin->x, ray->origin->y, ray->origin->z );
    MJ_COLLISION_RESULT_SIMPLE colResultSimple = MJ_NO_COLLISION;
    // Copy references so it's supposedly faster(?) when iterating
    unsigned int drawOrderCount = meshBS->drawOrderCount;
    unsigned short* drawOrderBuffer = meshBS->drawOrderBuffer;
    float* vertexBufferData = meshBS->vertexBufferData;

    mjVector3 rayDir(ray->dir);
    mjVector3 rayOrigin(*(ray->origin));

    for (unsigned i = 0; i < drawOrderCount; i+=3) {

        mjVector3 v0, v1, v2;
        v0.Set(vertexBufferData[(3*drawOrderBuffer[i])], vertexBufferData[1+(3*drawOrderBuffer[i])], vertexBufferData[2+(3*drawOrderBuffer[i])]);
        v1.Set(vertexBufferData[(3*(drawOrderBuffer[i+1]))], vertexBufferData[1+(3*(drawOrderBuffer[i+1]))], vertexBufferData[2+(3*(drawOrderBuffer[i+1]))]);
        v2.Set(vertexBufferData[(3*(drawOrderBuffer[i+2]))], vertexBufferData[1+(3*(drawOrderBuffer[i+2]))], vertexBufferData[2+(3*(drawOrderBuffer[i+2]))]);



        // Stolen from
        // https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm
        MJ_COLLISION_RESULT_SIMPLE colResult = MJ_NO_COLLISION;
        const float EPSILON = 0.0000001;
        mjVector3 edge1(v1);
        mjVector3 edge2(v2);
        mjVector3 h;
        mjVector3 s;
        mjVector3 q;
        float a,f,u,v;

        edge1.Subtract(v0);
        edge2.Subtract(v0);
        rayDir.CrossOut(edge2, &h);
        a = edge1.Dot(h);
        if (!(a > -MJ_EPSILON && a < MJ_EPSILON)) {
            //case false: This ray is parallel to this triangle.

            f = 1.0/a;
            s.CopyFrom(rayOrigin);
            s.Subtract(v0);
            u = f * (s.Dot(h));

            if (!(u < 0.0 || u > 1.0)) {
                //case false: projection outside of triangle

                s.CrossOut(edge1, &q);
                v = f * rayDir.Dot(q);
                if (!(v < 0.0 || u + v > 1.0)) {
                    //case false: projection outside of triangle

                    // At this stage we can compute t to find out where the intersection point is on the line.
                    float t = f * edge2.Dot(q);
                    if (t > 0) { // ray intersection
                        //LOGI("[%d] MJ_OVER", i);
                        colResult = MJ_OVER;
                    } else  { // This means that there is a line intersection but not a ray intersection.
                        // In motorJ we return "UNDER" because it's still useful.
                        //LOGI("[%d] MJ_UNDER", i);
                        colResult = MJ_UNDER;
                    }
                    colResultSimple = colResult;

                    if (poolHolder != nullptr) {

                        mjCollisionResult* out;
                        if (poolHolder->newCollisionResults.size() > 0) {
                            out = poolHolder->GetNextAvailableCollisionResult();
                        } else {
                            out = poolHolder->current;
                        }


                        out->relocationEffectObj0.type = MJ_COLLISION;
                        out->relocationEffectObj0.action = MJ_CHANGE_POSITION;
                        out->changeVelEffectObj0.type = MJ_COLLISION;
                        out->changeVelEffectObj0.action = MJ_ADD_VELOCITY;

                        /* FIXME:
                         *
                         * When collision response for meshes is implemented, uncomment this part.
                         * Otherwise it's just doing operations that will never be used

                        out->relocationEffectObj1 = new mjPhysicsEffect(MJ_COLLISION, MJ_CHANGE_POSITION);
                        out->changeVelEffectObj1 = new mjPhysicsEffect(MJ_COLLISION, MJ_ADD_VELOCITY);



                        out->relocationEffectObj1.type = MJ_COLLISION;
                        out->relocationEffectObj1.action = MJ_CHANGE_POSITION;
                        out->changeVelEffectObj1.type = MJ_COLLISION;
                        out->changeVelEffectObj1.action = MJ_ADD_VELOCITY; */

                        out->relocationEffectObj0.value.CopyFrom(*ray->origin);
                        out->relocationEffectObj0.value.ScaleAdd(t, ray->dir);


                        /* FIXME:
                         *
                         * When collision response for meshes is implemented, uncomment this part.
                         * Otherwise it's just doing operations that will never be used

                        out->relocationEffectObj1.value.CopyFrom(ray->dir);
                        out->relocationEffectObj1.value.MulScalar(-t); */

                        // calculate the triangle's normal

                        edge1.CrossOut(edge2, &out->changeVelEffectObj0.value);
                        out->changeVelEffectObj0.value.Normalize();

                        /* FIXME:
                         * When collision response for meshes is implemented, uncomment this part.
                         * Otherwise it's just doing operations that will never be used
                        out->changeVelEffectObj1.value.CopyFrom(h);
                        out->changeVelEffectObj1.value.MulScalar(-1); */


                        out->dist = t;
                        out->result = colResult;
                        poolHolder->newCollisionResults.push_back(out);
                        //LOGI("Added new CollResult: %d in stack", poolHolder->colResultPool->size());
                    }
                } else {
                    //LOGI("[%d](v < 0.0 || u + v > 1.0)", i);
                }
            } else {
                //LOGI("[%d](v < 0.0 || u + v > 1.0)", i);
            }
        } else {
            //LOGI("Parallel");
        }
    }
    //LOGI("End of test");
    return colResultSimple;
}
}
