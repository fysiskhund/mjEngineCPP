#include "mjMeshBS.h"

namespace mjEngine {


mjMeshBS::mjMeshBS()
    : mjBoundingStructure()
{
#ifndef NODEBUGDELETEMSG
    LOGI("mjMeshBS() %p", this);
#endif
    this->isImmovable = true;
    this->type= MJ_MESH_BS;


}

mjMeshBS::~mjMeshBS()
{
#ifndef NODEBUGDELETEMSG
    LOGI("~mjMeshBS() %p", this);
#endif
}

void mjMeshBS::SetData(unsigned int drawOrderCount, unsigned short* drawOrderBuffer, float* vertexBufferData, unsigned int numVertices)
{
    this->drawOrderCount = drawOrderCount;
    this->drawOrderBuffer = drawOrderBuffer;
    this->vertexBufferData = vertexBufferData;
    this->numVertices = numVertices;

    //LOGI("Vertex orders: ");
    //for (int i = 0; i < drawOrderCount; i++) {
    //    LOGI("%d", drawOrderBuffer[i]);
    //}
    //LOGI(";");

    /*LOGI("Vertices in order:");
    for (int i = 0; i < drawOrderCount; i++) {
            LOGI("[%d]: %3.3f[%d], %3.3f, %3.3f", drawOrderBuffer[i], vertexBufferData[0+(3*drawOrderBuffer[i])], vertexBufferData[1+(3*drawOrderBuffer[i])], vertexBufferData[2+(3*drawOrderBuffer[i])]);
        }*/
}



}
