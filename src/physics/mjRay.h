#ifndef MJRAY_H
#define MJRAY_H

#include "mjBoundingStructure.h"
#include "../core/mjVector3.h"

namespace mjEngine{

class mjRay : public mjBoundingStructure
{
public:
    mjVector3* origin;
    mjVector3 dir;

    mjRay(mjVector3* origin, mjVector3& dir);
    virtual ~mjRay();
};

}

#endif // MJRAY_H
