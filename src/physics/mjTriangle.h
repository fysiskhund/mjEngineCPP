#ifndef MJTRIANGLE_H
#define MJTRIANGLE_H

#include "mjBoundingStructure.h"
#include "../core/mjVector3.h"

namespace mjEngine{

class mjTriangle: public mjBoundingStructure
{
public:
    mjVector3* v0 = nullptr;
    mjVector3* v1 = nullptr;
    mjVector3* v2 = nullptr;

    bool isStatic = true;

    //! Create a new triangle. Its 3 vertices MUST be specified before adding it to the physics engine!
    mjTriangle();

    //! Create a new triangle as specified
    mjTriangle(mjVector3* v0, mjVector3* v1, mjVector3* v2);

    virtual ~mjTriangle();

    //! Specify the triangle's data
    void SetData(mjVector3* v0, mjVector3* v1, mjVector3* v2);

    //! Get the triangle's normal. If the triangle rotates then you need to call UpdateNormal() before attempting to get the normal.
    void Normal(mjVector3* normalOut);

    //! Update the internal normal variable
    void UpdateNormal();
private:
    mjVector3 normal;
};

}
#endif // MJTRIANGLE_H
