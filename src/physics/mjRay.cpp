#include "mjRay.h"


namespace mjEngine{
mjRay::mjRay(mjVector3* origin, mjVector3& dir)
{
#ifndef NODEBUGDELETEMSG
    LOGI("mjRay() %p", this);
#endif

    this->origin = origin;
    this->dir.CopyFrom(dir);
    this->type = MJ_RAY;
}

mjRay::~mjRay()
{
#ifndef NODEBUGDELETEMSG
    LOGI("~mjRay() %p", this);
#endif
}


}
