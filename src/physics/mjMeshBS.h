#ifndef MJMESHBS_H
#define MJMESHBS_H

#include "mjBoundingStructure.h"
#include "../util/mjLog.h"

namespace mjEngine {

class mjMeshBS : public mjBoundingStructure
{
public:
    unsigned int drawOrderCount = 0;
    unsigned short* drawOrderBuffer = nullptr;
    float* vertexBufferData = nullptr;
    unsigned int numVertices = 0;


    mjMeshBS();
    virtual ~mjMeshBS();
    void SetData(unsigned int drawOrderCount, unsigned short* drawOrderBuffer, float* vertexBufferData, unsigned int numVertices);
};

}
#endif // MJMESHBS_H
