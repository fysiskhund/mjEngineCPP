#ifndef MJCOLLISIONSTRUCTURE
#define MJCOLLISIONSTRUCTURE

namespace mjEngine{

enum structuretype
{
    MJ_RAY,
	MJ_SPHERE,
	MJ_AABB,
	MJ_TRIANGLE,
    MJ_MESH_BS,
    MJ_NO_BOUNDING_STRUCT
};

class mjBoundingStructure
{
public:
    float elasticCoefficient = 1;
	structuretype type;
	bool isImmovable = false;
};

}


#endif
