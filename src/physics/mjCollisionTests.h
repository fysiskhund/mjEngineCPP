/*
 * Copyright (C) 2014 Alejandro Valenzuela Roca
 */


#ifndef MJCOLLISIONTESTS_H
#define MJCOLLISIONTESTS_H

#include <math.h>
#include <vector>

#include "../core/mjVector3.h"
#include "../core/mjConstants.h"
#include "mjCollisionResult.h"
#include "mjSphere.h"
#include "mjAABB.h"
#include "mjTriangle.h"
#include "mjRay.h"
#include "mjMeshBS.h"
#include "mjCollisionResultPoolHolder.h"

#include "../extLibs/math/mjMathHelper.h"


#include "../extLibs/logger/mjLog.h"


namespace mjEngine
{



class mjCollisionTests{
public:

    static MJ_COLLISION_RESULT_SIMPLE SphereVsSphere(mjSphere* s0, mjSphere* s1, mjCollisionResult* out);

    static MJ_COLLISION_RESULT_SIMPLE AABBVsAABB(mjAABB* a, mjAABB* b, mjCollisionResult* out);

    static MJ_COLLISION_RESULT_SIMPLE SphereVsAABB(mjSphere* s0, mjAABB* aabb0, mjCollisionResult* out);

    static MJ_COLLISION_RESULT_SIMPLE RayVsTriangle(mjRay* ray, mjTriangle* tr, mjCollisionResult* out);

    static MJ_COLLISION_RESULT_SIMPLE RayVsMeshBS(mjRay* ray, mjMeshBS* meshBS, mjCollisionResultPoolHolder* poolHolder);

private:
};
}

#endif
