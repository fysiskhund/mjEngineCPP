#include "mjTriangle.h"

namespace mjEngine {


mjTriangle::mjTriangle()
{
#ifndef NODEBUGDELETEMSG
    LOGI("mjTriangle() %p", this);
#endif
    this->type = MJ_TRIANGLE;
    isImmovable = true;

}

mjTriangle::mjTriangle(mjVector3* v0, mjVector3* v1, mjVector3* v2)
{
#ifndef NODEBUGDELETEMSG
    LOGI("mjTriangle() %p", this);
#endif


    this->isImmovable = true; // for the time being, all triangles are immovable (i.e. not affected by collisions).
    SetData(v0, v1, v2);
    this->type = MJ_TRIANGLE;

}

mjTriangle::~mjTriangle()
{
#ifndef NODEBUGDELETEMSG
    LOGI("~mjTriangle() %p", this);
#endif

}

void mjTriangle::SetData(mjVector3* v0, mjVector3* v1, mjVector3* v2) {

    //this->isStatic = isStatic; // The triangle vertices will not change overtime

    this->v0 = v0;
    this->v1 = v1;
    this->v2 = v2;

    UpdateNormal();
}


void mjTriangle::Normal(mjVector3* normalOut)
{
    normalOut->CopyFrom(normal);
}

void mjTriangle::UpdateNormal()
{
    mjVector3 v0ToV1(*v1);
    v0ToV1.Subtract(*v0);

    mjVector3 v2ToV1(*v2);
    v2ToV1.Subtract(*v0);

    normal.x = (v0ToV1.y*v2ToV1.z) - (v0ToV1.z*v2ToV1.y);
    normal.y = (v0ToV1.z*v2ToV1.x) - (v0ToV1.x*v2ToV1.z);
    normal.z = (v0ToV1.x*v2ToV1.y) - (v0ToV1.y*v2ToV1.x);

    float norm = normal.GetNorm();
    normal.MulScalar(1.0/norm);

}

}
